import urllib.request
import urllib.parse
from bs4 import BeautifulSoup
from urllib.parse import unquote
from urllib import error
import re
import json
from pprint import pprint
import math
import time


def _get_page(product_name, page_number):
    try:
        temp = re.sub("\s", "+", product_name)
        temp = urllib.parse.quote(temp.encode('utf-8'))
        if (page_number > 1):
            url = f'https://www.lidl.es/es/search?query={temp}&filterAvailability=online&wh=2&page={page_number}'
        else:
            url = f'https://www.lidl.es/es/search?query={temp}'

        html = urllib.request.urlopen(url).read().decode('utf-8')

        return BeautifulSoup(html, "html.parser")
    except Exception as e:
        raise e


def lidl_scrap(jsonObj:bool, product_name="*", page_number=1):
    '''
    Consults spanish lidl's website and retrieves product information.
        
        Parameters:
            jsonObj: If false the product information comes as a dict of dicts, otherwise it comes as a dict of json objects.
            product_name(str): The product you are looking for.
            page_number(int): Number of the page you want to start scrapping.
            products per page(int): Number of products of a page, could be used to slow the scrapper as it takes 10 seconds off for each page, so it is harder to detect.
        
        Returns:
            result(dict): A dictionary that contains a key 'remain' which is used to let the user know how many pages are
            left to scrap and a key 'products' that contains the information extracted from the products in the current page
            , presented as a dict such as this one:
            product_info(dict): Dict of keys: name(str),brand(str) and price(decimal number).
            
            If the product was not found returns an empty dict.
    '''
    if(product_name.lstrip().rstrip() == '' or len(product_name.lstrip().rstrip()) == 0):
        product_name = '*'
    try:
        if (isinstance(product_name, str) and isinstance(page_number, int)):
            result = dict()
            soup = _get_page(product_name, page_number)

            query_product = re.sub("\s", "+", product_name)

            result_count = soup.find_all('p', class_='result_count')

            #If no results were found return empty results dict.
            if(len(result_count) <= 0):
                result['remain'] = 0
                result['products'] = dict()
                return result
            total_product_number = int(
                re.sub('[^0-9]*', '', result_count[0].text.lstrip()))

            number_of_pages = math.ceil(total_product_number / 24)

            if (number_of_pages - page_number <= 0 and total_product_number > 0):
                number_of_pages = 1
            else:
                number_of_pages = number_of_pages - page_number

            products = []

            soup = _get_page(product_name, page_number)

            tags = soup.find_all('a', id=re.compile('product_.*'))

            for tag in tags:
                product_info = dict()
                attrs_dict = json.loads(unquote(tag.attrs['data-impressiondata']))
                if not jsonObj:
                    name = attrs_dict['name'].replace('+', ' ') if 'name' in attrs_dict.keys() else ''
                    brand = attrs_dict['brand'].replace('+', ' ') if 'brand' in attrs_dict.keys() else ''
                    price = attrs_dict['price'] if 'price' in attrs_dict.keys() else '0.00'
                    product_info['name'] = name.lstrip() if isinstance(name, str) else name
                    product_info['brand'] = brand.lstrip() if isinstance(brand, str) else brand
                    product_info['price'] = price
                products.append(attrs_dict if jsonObj else product_info)

            page_number += 1

            result['remain'] = number_of_pages - page_number
            result['products'] = products

            return result
        else:
            raise TypeError(
                "product_name should be a string, page_number should be an integer and products_per_page should also be an integer.")
    except urllib.error.HTTPError as e:
        raise e


def main():
    try:
        jsonObj = True
        query = 'alleta'
        page_number = 1
        products_per_page = 12

        while(True):
            lidl = lidl_scrap(jsonObj, query, page_number)
            show_products(jsonObj, lidl['products'])
            if(lidl['remain'] <= 0):
                break
            time.sleep(10)
            page_number += 1

    except urllib.error.HTTPError as e:
        print(e)
    except TypeError as e:
        print(e)

def show_products(jsonObj:bool,products:dict):
    for product_info in products:
        if jsonObj:
            pprint(product_info)
            print()
        else:
            print(f'{product_info["name"]}; brand: {product_info["brand"]}; price:{product_info["price"]}€')

if __name__ == '__main__':
    main()
