import urllib.request
import urllib.parse
from urllib.parse import unquote
import re
import json
import time
import smtplib
from pprint import pprint

categories_names=\
{'agriculture & industrial',
 'appliances',
 'baby & child',
 'bikes',
 'cars',
 'cell phones & accessories',
 'collectibles & art',
 'computers & electronic',
 'construction',
 'fashion & accessories',
 'games & consoles',
 'home & garden',
 'jobs',
 'motorbike',
 'motors & accessories',
 'movies, books & music',
 'other',
 'real estate',
 'services',
 'sports & leisure',
 'tv, audio & cameras'}

wallapop_categories='https://api.wallapop.com/api/v3/categories'

wallapop_categories_json = json.loads(urllib.request.urlopen(wallapop_categories).read().decode())

categories = dict()

for i in wallapop_categories_json['categories']:
    categories[i['name'].lower()] = i['id']

pprint(categories)

category = 'computers & electronic'
search_query = 'ratón'
max_sale_price = None
min_sale_price = None

wallapop_query = f'https://api.wallapop.com/api/v3/general/search?category_ids={categories[category]}&keywords={urllib.parse.quote_plus(search_query.encode())}&filters_source=search_box&'+('max_sale_price={max_sale_price}' if max_sale_price != None and max_sale_price>= 0 else '')+('&min_sale_price={min_sale_price}' if min_sale_price != None and min_sale_price >= 0 else '')
jsonobj = urllib.request.urlopen(wallapop_categories).read().decode()

json = json.loads(jsonobj)['search_objects']

for i in json:
    print(f'Name: {i["title"]}, price: {i["price"]}{i["currency"]}, is_reserved: {i["flags"]["reserved"]}')
# for i in categories:
#     print(f'id: {i},name: {categories[i]}')

# json = json.loads(jsonobj)['search_objects']
#
# blacklist = ['poster','amiibo','guia','guía']
#
# for i in json:
#     res = []
#     for j in blacklist:
#         res.append(j in i['description'].lower() or j in i['title'].lower())
#     if not(True in res):
#         if 'Breath of the wild'.lower() in i['title'].lower() and 30 >= i['price'] > 20 and i['currency'] == 'EUR' and i['flags']['reserved'] == False:
#             print(f'Name: {i["title"]}, price: {i["price"]}{i["currency"]}, is_reserved: {i["flags"]["reserved"]}')
